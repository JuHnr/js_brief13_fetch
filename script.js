async function getUsers() {
    // appelle l'API et récupère la réponse
    const response = await fetch("https://jsonplaceholder.typicode.com/users");
    // de la réponse, on garde uniquement le JSON, qui sera converti en JavaScript
    const userList = await response.json();

    //pour chaque personne du tableau userList
    for (const user of userList) {
        //on affiche les informations : nom, rue, code postal, ville
        console.log(`${user.name}, ${user.address.street}, ${user.address.zipcode}, ${user.address.city}`);
    }
}

//appel de la fonction
getUsers();